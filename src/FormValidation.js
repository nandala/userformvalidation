import React, {Component} from 'react';
import './formStyle.css';

export default class Test extends React.Component {
    constructor(props){
      super(props);
  
      this.state = {
        fields: {},
        errors: {},
        displayData :false
      }
    }
  
    handleValidation(){
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
  
      //Name
      if(!fields["name"]){
        formIsValid = false;
        errors["name"] = "Name cannot be empty";
      }
  
      if(typeof fields["name"] !== "undefined"){
        if(!fields["name"].match(/^[a-zA-Z]+$/)){
          formIsValid = false;
          errors["name"] = "Please enter valid name";
        }      	
      }
  
      //Email
      if(!fields["email"]){
          formIsValid = false;
          errors["email"] = "Email cannot be empty";
        }
                      if(typeof fields["email"] !== "undefined"){
                        let lastAtPos = fields["email"].lastIndexOf('@');
                        let lastDotPos = fields["email"].lastIndexOf('.');
                  
                        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                          formIsValid = false;
                          errors["email"] = "Please eneter valid Email";
                        }
                      }

   
      if(!fields["phone"]){
        formIsValid = false;
        errors["phone"] = "Phone cannot be empty";
      }
      if(typeof fields["phone"] !== "undefined"){
        if(!fields["phone"].match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/
        )){
          formIsValid = false;
          errors["phone"] = "Please enter valid 10digits number";
        }      	
      }

      if(!fields["address"]){
        formIsValid = false;
        errors["address"] = "Address cannot be empty";
      }

      if(typeof fields["address"] !== "undefined"){
        if(!fields["address"].match(/^[a-zA-Z0-9\s,'-]*$/
)){
          formIsValid = false;
          errors["address"] = "Please enter valid address";
        }      	
      }

      if(!fields["company"]){
        formIsValid = false;
        errors["company"] = "Company cannot be empty";
      }

      if(typeof fields["company"] !== "undefined"){
        if(!fields["company"].match(/^[a-zA-Z0-9\s,'-]*$/
)){
          formIsValid = false;
          errors["company"] = "Please enter valid company";
        }      	
      }

      this.setState({errors: errors});
      return formIsValid;
    }
  
    contactSubmit(e){
      e.preventDefault();
      if(this.handleValidation()){
        this.setState({displayData :true})
      }else{
        this.setState({displayData :false})

      }
  
    }
  
    handleChange(field, e){    		
      let fields = this.state.fields;
      fields[field] = e.target.value;        
      this.setState({fields});
    }
  
    render(){
      return (
          !this.state.displayData ?
        <div className ="text-align">     
        <div className ="padding">USER FORM VALIDATION</div>   	
          <form name="contactform" className="contactform" onSubmit= {this.contactSubmit.bind(this)}>
            <div className="col-md-12">
              <fieldset>
                Name:
                 <input className="m-l-100" ref="name" type="text" size="30" placeholder="Name" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]}/>
                <br />
                <span className="error">{this.state.errors["name"]}</span>
                <br/>
                Email:<input refs="email" className="m-l-100" type="text" size="30" placeholder="Email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]}/>
                <br />
                <span className="error">{this.state.errors["email"]}</span>
                <br/>
                Phone:
                <input refs="phone" type="text" size="30"  className="m-l-100" placeholder="Phone" onChange={this.handleChange.bind(this, "phone")} value={this.state.fields["phone"]}/>
                <br/>
                <span className="error">{this.state.errors["phone"]}</span>
                <br/>
                Address:<input refs="address" type="text" size="30" className="m-l-100" placeholder="Address" onChange={this.handleChange.bind(this, "address")} value={this.state.fields["address"]}/>
                <br/>
                <span className="error">{this.state.errors["address"]}</span>
                <br/>
                Working company:<input refs="company" type="text" className="m-l-30" size="30" placeholder="Company" onChange={this.handleChange.bind(this, "company")} value={this.state.fields["company"]}/>
                <br/>
                <span className="error">{this.state.errors["company"]}</span>
                <br/>
              </fieldset>
            </div>
            <div className="col-md-12">
              <fieldset>
                <button className="btn btn-lg pro" id="submit" value="Submit">SUBMIT</button>
              </fieldset>
            </div>
          </form>
        </div>
     :<div className ="col-md-12 col-lg-12 col-sm-12 text-align">
        <div  className ="col-md-12 col-lg-12 col-sm-12 padding">
        Name: {this.state.fields["name"] }
        </div>
        <div  className ="col-md-12 col-lg-12 col-sm-12 padding">
        Email:{this.state.fields["email"] }
        </div>
        <div  className ="col-md-12 col-lg-12 col-sm-12 padding">
        Company: {this.state.fields["company"] }
        </div>
        <div  className ="col-md-12 col-lg-12 col-sm-12 padding">
        Phone: {this.state.fields["phone"] }
        </div>
        <div  className ="col-md-12 col-lg-12 col-sm-12 padding">
        Address: {this.state.fields["address"] }
        </div>
        <div>
        </div>
     </div>
     )
    }
  }
  
//   ReactDOM.render(<Test />, document.querySelector("#app"))
  